package main

const (
	c_PowerPlugStatusPath = "/sys/class/power_supply/AC/online"
	c_BatteryRootPath     = "/sys/class/power_supply/" // BAT[0-9]*
	c_BatteryEventFile    = "uevent"
)
