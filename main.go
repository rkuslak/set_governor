package main

func main() {
	options := ParseCommandLine()

	policies := GetPolicies()

	if options.DesiredGovernor != "" {
		for _, policy := range policies {
			policy.SetGovernor(options.DesiredGovernor)
		}
	}

	if options.DesiredPreference != "" {
		for _, policy := range policies {
			policy.SetEnergyPreference(options.DesiredPreference)
		}
	}

	if (options.DesiredGovernor == "" && options.DesiredPreference == "") || options.Verbose {
		printPolicies(policies)
	}
}
