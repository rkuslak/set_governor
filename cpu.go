package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

const (
	UserField = iota
	NiceField
	SystemField
	IOWaitField
	IdleField
	SoftIRQField
	StealField
	GuestField
	GuestNiceField
)

func GetCPUUsage() (uint64, uint64) {
	var idle, total uint64

	stat_data, err := ioutil.ReadFile("/proc/stat")
	if err != nil {
		return idle, total
	}

	lines := strings.Split(string(stat_data), "\n")
	for _, line := range lines {
		fields := strings.Fields(line)

		if len(fields) < 2 || fields[0] != "cpu" {
			continue
		}

		for idx, field := range fields[1:] {
			fieldValue, err := strconv.ParseUint(field, 10, 64)
			if err != nil {
				fmt.Printf("Failed to parse field %s, skipping\n", field)
				continue
			}

			total += fieldValue
			if idx == IdleField || idx == IOWaitField {
				idle += fieldValue
			}
		}
	}

	return idle, total
}

func getSomeCPUDeltas() {
	deltaIdle, deltaTotal := GetCPUUsage()

	for x := 0; x < 10; x++ {
		time.Sleep(2 * time.Second)
		idle, total := GetCPUUsage()

		ticksAvailable := total - deltaTotal
		ticksIdle := idle - deltaIdle
		deltaIdle = idle
		deltaTotal = total

		percentUsed := (ticksAvailable - ticksIdle) * 100 / ticksAvailable
		fmt.Printf("CPU usage: %d%%\n", percentUsed)
	}
}
