package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

const (
	c_PoliciesRootPath            = "/sys/devices/system/cpu/cpufreq/"
	c_AvailableGovernorsFile      = "scaling_available_governors"
	c_CurrentGovernorFile         = "scaling_governor"
	c_CurrentEnergyProfileFile    = "energy_performance_preference"
	c_AvailableEnergyProfilesFile = "energy_performance_available_preferences"
)

const (
	c_PolicyPrintFormat = "%-20s %-20s %-40s %-20s %-40s\n"
)

func printPolicies(policies []Policy) {
	fmt.Printf(c_PolicyPrintFormat, "POLICY", "CURRENT GOV", "AVAILABLE GOV", "CURRENT PREF", "AVAILABLE")

	for _, policy := range policies {
		availableGovernors := strings.Join(policy.GetGovernors(), ", ")
		availablePreferences := strings.Join(policy.GetEnergyPreferences(), ", ")
		fmt.Printf(c_PolicyPrintFormat, policy.Name, policy.GetGovernor(), availableGovernors, policy.GetEnergyPreference(), availablePreferences)
	}
}

// Policy reprensts the state of a given Policy, which normally dictates the
// Governors running on a given CPU
type Policy struct {
	Name            string
	PolicyDirectory string

	currentGovernor    string
	availableGovernors []string

	currentEnergyProfile    string
	availableEnergyProfiles []string
}

// GetPolicies returns a array of all available Policies on the computer
// relevent to our actions.
func GetPolicies() []Policy {
	availablePolicies := []Policy{}

	walkFunc := func(policyPath string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			return nil
		}

		testPath := path.Join(policyPath, c_AvailableGovernorsFile)
		_, err = os.Stat(testPath)
		if err != nil {
			return nil
		}

		name := path.Base(policyPath)
		policy := Policy{
			Name:            name,
			PolicyDirectory: policyPath,
		}
		availablePolicies = append(availablePolicies, policy)
		return nil
	}

	err := filepath.Walk(c_PoliciesRootPath, walkFunc)
	if err != nil {
		panic(err)
	}

	return availablePolicies
}

// GetGovernor returns the current Governor for this given Policy
func (policy *Policy) GetGovernor() string {
	if policy.currentGovernor == "" {
		// If we have not previously polled policy or it has been unset on our
		// struct, get the current policy and save it.
		governorPath := path.Join(policy.PolicyDirectory, c_CurrentGovernorFile)

		governorFile, err := ioutil.ReadFile(governorPath)
		if err != nil {
			panic("Failed to read policy file!!!")
		}
		policy.currentGovernor = strings.TrimSpace(string(governorFile))
	}

	return policy.currentGovernor
}

// GetGovernor updates the current Governor for this given Policy. After update,
// later calls to GetGovenor should reflect the new value, if changed.
func (policy *Policy) SetGovernor(newGovernor string) {
	governorPath := path.Join(policy.PolicyDirectory, c_CurrentGovernorFile)
	info, err := os.Stat(governorPath)

	if err != nil {
		msg := fmt.Sprintf("Failed to load governor file %s", governorPath)
		panic(msg)
	}

	policy.currentGovernor = ""
	ioutil.WriteFile(governorPath, []byte(newGovernor), info.Mode())
}

// GetGovernors returns the available Governors for this given Policy as an
// array of strings.
func (policy *Policy) GetGovernors() []string {
	governorsPath := path.Join(policy.PolicyDirectory, c_AvailableGovernorsFile)

	governorsFile, err := ioutil.ReadFile(governorsPath)
	if err != nil {
		panic("Failed to read policy file!!!")
	}

	availableGovernorsLine := strings.TrimSpace(string(governorsFile))
	policy.availableGovernors = strings.Split(availableGovernorsLine, " ")

	return policy.availableGovernors
}

// GetGovernor returns the current Governor for this given Policy
func (policy *Policy) GetEnergyPreference() string {
	if policy.currentEnergyProfile == "" {
		// If we have not previously polled policy or it has been unset on our
		// struct, get the current policy and save it.
		preferencePath := path.Join(policy.PolicyDirectory, c_CurrentEnergyProfileFile)

		preferenceFile, err := ioutil.ReadFile(preferencePath)
		if err != nil {
			panic("Failed to read policy file!!!")
		}
		policy.currentEnergyProfile = strings.TrimSpace(string(preferenceFile))
	}

	return policy.currentEnergyProfile
}

// GetGovernor updates the current Governor for this given Policy. After update,
// later calls to GetGovenor should reflect the new value, if changed.
func (policy *Policy) SetEnergyPreference(newPreference string) {
	preferencePath := path.Join(policy.PolicyDirectory, c_CurrentEnergyProfileFile)
	info, err := os.Stat(preferencePath)

	if err != nil {
		msg := fmt.Sprintf("Failed to load energy profile file %s", preferencePath)
		panic(msg)
	}

	ioutil.WriteFile(preferencePath, []byte(newPreference), info.Mode())
}

// GetGovernors returns the available Governors for this given Policy as an
// array of strings.
func (policy *Policy) GetEnergyPreferences() []string {
	preferencesPath := path.Join(policy.PolicyDirectory, c_AvailableEnergyProfilesFile)

	preferencesFile, err := ioutil.ReadFile(preferencesPath)
	if err != nil {
		panic("Failed to read policy file!!!")
	}

	availablePreferencesLine := strings.TrimSpace(string(preferencesFile))
	policy.availableEnergyProfiles = strings.Split(availablePreferencesLine, " ")

	return policy.availableEnergyProfiles
}
