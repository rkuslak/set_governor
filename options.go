package main

import "os"

import "strings"

type Options struct {
	DesiredGovernor   string
	DesiredPreference string
	Verbose           bool
}

func ParseCommandLine() *Options {
	options := Options{
		Verbose: false,
	}

	for idx := 1; idx < len(os.Args); idx++ {
		switch strings.ToLower(os.Args[idx]) {
		case "--verbose", "-v":
			options.Verbose = true
		case "-p", "--preference":
			idx++
			if idx < len(os.Args) {
				options.DesiredPreference = os.Args[idx]
			} else {
				panic("Must specify a preference if using --preference!")
			}
		default:
			options.DesiredGovernor = os.Args[idx]
		}
	}

	return &options
}
