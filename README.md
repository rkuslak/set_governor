# set_governor - A simple script to show and update the scaling governors on CPUs in Linux.

# USAGE
Pass desired policy wanted to set. You will likely need to be root or use sudo to do this. Pass `--verbose` to have it print the current settings after update.

To print the current governors and available ones, run the script without arguments.

## TODO
* Make this readme actual good
* Provide server to allow setting/changing from normal user?
